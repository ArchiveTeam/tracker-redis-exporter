use prometheus::Registry;
use crate::client_manager::ClientManager;
use crate::models::TrackerInfo;

pub mod project_metrics;
pub mod queue_metrics;
pub mod downloader_metrics;

pub trait MetricsProvider: Sync + Send {
    fn get_registry(&self) -> color_eyre::Result<&Registry>;
    fn update(&self, client_mgr: &ClientManager, project: &str, info: &TrackerInfo) -> color_eyre::Result<()>;
    fn get_metric_type_name(&self) -> &str;
}
