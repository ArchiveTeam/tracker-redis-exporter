use std::collections::BTreeMap;
use prometheus::{IntGaugeVec, Opts, Registry};
use redis::Commands;
use crate::client_manager::{ClientManager, RedisConnection};
use crate::metrics::MetricsProvider;
use crate::models::TrackerInfo;

fn update_downloader_gauge_from_redis(
    con: &mut RedisConnection,
    project: &str,
    redis_key: &str,
    metric: &IntGaugeVec,
) -> color_eyre::Result<()> {
    let k = format!("{project}:{redis_key}");
    let m: BTreeMap<String, i64> = con.hgetall(k)?;
    for (downloader_name, value) in m {
        let m = metric.get_metric_with_label_values(&[project, downloader_name.as_str()])?;
        m.set(value);
    }
    Ok(())
}

pub struct DownloaderMetrics {
    registry: Registry,
    downloader_count: IntGaugeVec,
    downloader_bytes: IntGaugeVec,
}

impl DownloaderMetrics {
    pub fn new() -> color_eyre::Result<Self> {
        let registry = Registry::new_custom(Some("at_tracker".to_string()), None)?;

        let s = Self {
            registry,
            downloader_count: IntGaugeVec::new(Opts::new("downloader_count", "downloader_count"), &["project", "downloader_name"])?,
            downloader_bytes: IntGaugeVec::new(Opts::new("downloader_bytes", "downloader_bytes"), &["project", "downloader_name"])?,
        };

        s.registry.register(Box::new(s.downloader_count.clone()))?;
        s.registry.register(Box::new(s.downloader_bytes.clone()))?;

        Ok(s)
    }
}

impl MetricsProvider for DownloaderMetrics {
    fn get_registry(&self) -> color_eyre::Result<&Registry> {
        Ok(&self.registry)
    }

    fn update(&self, client_mgr: &ClientManager, project: &str, info: &TrackerInfo) -> color_eyre::Result<()> {
        let mut con = client_mgr.get_connection_from_tracker(info)?;

        update_downloader_gauge_from_redis(&mut con, project, "downloader_count", &self.downloader_count)?;
        update_downloader_gauge_from_redis(&mut con, project, "downloader_bytes", &self.downloader_bytes)?;

        Ok(())
    }

    fn get_metric_type_name(&self) -> &str {
        "downloader"
    }
}
