use clap::Parser;
use regex::Regex;
use crate::models::TrackerRedisInfo;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(short, long, default_value = "redis://127.0.0.1:6379")]
    master_redis: url::Url,

    #[arg(short, long, default_value = "http://127.0.0.1:9091")]
    pub push_url: url::Url,

    #[arg(short, long)]
    pub included_projects: Vec<Regex>,

    #[arg(short, long)]
    pub excluded_projects: Vec<Regex>,

    #[arg(long, default_value = "600")]
    pub projects_refresh_interval: usize,

    #[arg(long, default_value = "3")]
    pub projects_inactive_minutes: u64,

    #[arg(long, default_value = "60")]
    pub downloader_metrics_refresh_interval: usize,

    #[arg(long, default_value = "15")]
    pub queue_metrics_refresh_interval: usize,

    #[arg(long, default_value = "15")]
    pub project_metrics_refresh_interval: usize,
}

impl Args {
    pub fn get_master_redis_info(&self) -> TrackerRedisInfo {
        TrackerRedisInfo::from(self.master_redis.clone())
    }
}
