use std::collections::BTreeMap;
use serde::Deserialize;
use url::Url;

#[derive(Debug, Clone, Deserialize, Hash, Eq, PartialEq)]
pub struct TrackerRedisInfo {
    pub host: String,
    pub pass: Option<String>,
    pub port: u16,
}

impl From<Url> for TrackerRedisInfo {
    fn from(value: Url) -> Self {
        let port = value.port().unwrap_or(6379);
        let host = value.host().map(|v| v.to_string()).unwrap_or_else(|| "127.0.0.1".to_string());
        let pass = value.password().map(|v| v.to_string());
        Self {
            host,
            pass,
            port,
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct TrackerOffloadInfo {
    pub high: usize,
    pub low: usize,
    pub pipelinesize: Option<usize>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct TrackerInfo {
    pub title: String,
    pub min_script_version: String,
    pub redis: Option<TrackerRedisInfo>,
    pub max_claims_hard: Option<usize>,
    pub max_claims_soft: Option<usize>,
    pub offload: Option<TrackerOffloadInfo>,
}

pub type TrackerMap = BTreeMap<String, TrackerInfo>;
