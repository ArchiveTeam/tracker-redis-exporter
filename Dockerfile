# Build Stage
FROM rust:1.71.1-bookworm AS builder
RUN apt-get update \
    && apt-get install -y openssl ca-certificates tini libssl3 libssl-dev build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /usr/src/tracker-redis-exporter
COPY Cargo.toml .
COPY src src
RUN cargo build --release

FROM debian:bookworm
WORKDIR /app

RUN apt-get update \
    && apt-get install -y openssl ca-certificates tini libssl3 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copy bin from builder to this new image
COPY --from=builder /usr/src/tracker-redis-exporter/target/release/tracker-redis-exporter /app/tracker-redis-exporter

# Default command, run app
ENTRYPOINT ["/usr/bin/tini-static", "--", "/app/tracker-redis-exporter"]
